<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Stripe</title>
	</head>
	<body>
		<?php
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');
			require_once(__DIR__.'/inc.php');
		?>
		
		<h1>Stripe subaccount new link</h1>
		
		<input type="text" id="vs_stripeaccid" name="vs_stripeaccid" value="acct_1JSjHVRd08coD59b">
		<br/><br/>
		<button id="GoToPayStripeButt6">GET Link</button>
		<br/><br/>
		<div id="vs_res6"></div>
		
		<script>
			document.addEventListener("DOMContentLoaded", function(){
				var ajaxURL            = '/charge6.php';
				var vs_res6            = document.getElementById('vs_res6');
				var vs_stripeaccid     = document.getElementById('vs_stripeaccid');
				var GoToPayStripeButt6 = document.getElementById('GoToPayStripeButt6');
				
				GoToPayStripeButt6.addEventListener("click", function(){
					fetch(ajaxURL, {
						method:      'POST',
						credentials: 'same-origin',
						headers: {
							'Content-Type':  'application/x-www-form-urlencoded',
							'Cache-Control': 'no-cache',
						},
						body: new URLSearchParams({
							vs_stripeaccid: vs_stripeaccid.value,
						})
					}).then(response=>response.json())
					.then(
						response=>{
							console.log(response);
							vs_res6.innerHTML = '<br/>url:'+response.url;
						}
					)
					.catch(err=>console.log(err));
				});
				
			});
		</script>
		
	</body>
</html>