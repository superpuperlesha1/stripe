<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	require_once(__DIR__.'/inc.php');
	
	require_once(dirname(__FILE__).'/stripe/vendor/autoload.php');
	\Stripe\Stripe::setApiKey(sk_test);
	
	$token = $_POST['vs_token'] ?? '';
	
	$charge = \Stripe\Charge::create([
		'amount'      => 57,
		'currency'    => 'usd',
		'description' => 'Example charge !!',
		'source'      => $token,
	]);
	
	echo json_encode($charge);
	
?>