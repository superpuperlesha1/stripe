<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Stripe</title>
	</head>
	<body>
		<?php
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');
			require_once(__DIR__.'/inc.php');
		?>
		
		<h1>Stripe create subaccount</h1>
		
		<form action="#uID" method="post">
			<input type="text" id="vs_email" name="vs_email" value="123@gmail.com">
			<br/><br/>
			<a href="#uID" id="vs_submit">Create new account</a>
			<div id="vs_res"></div>
		</form>
		
		<script>
			document.addEventListener("DOMContentLoaded", function(event){
				WPajaxURL     = '/charge3.php';
				var vs_res    = document.getElementById('vs_res');
				var vs_email  = document.getElementById('vs_email');
				var vs_submit = document.getElementById('vs_submit');
				
				vs_submit.addEventListener('click', function(event){
					fetch(WPajaxURL, {
						method:      'POST',
						credentials: 'same-origin',
						headers:{
							'Content-Type':  'application/x-www-form-urlencoded',
							'Cache-Control': 'no-cache',
						},
						body: new URLSearchParams({
							vs_email: vs_email.value,
						})
					}).then(response=>response.json())
					.then(
						response=>{
							console.log(response);
							vs_res.innerHTML = '<br/>id:'+response.id+' <br/>url:'+response.url;
						}
					)
					.catch(err=>console.log(err));
				});
			});
		</script>
		
	</body>
</html>