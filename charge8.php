<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once(__DIR__.'/inc.php');

require_once(dirname(__FILE__).'/stripe/vendor/autoload.php');
\Stripe\Stripe::setApiKey(sk_test);

$source_id   = $_POST['source_id']   ?? '';
$email       = $_POST['email']       ?? '';
$customer_id = $_POST['customer_id'] ?? '';
$price       = $_POST['price']       ?? 50;

if( !$customer_id ){
    $stripe = new \Stripe\StripeClient(sk_test);
    $customer = $stripe->customers->create(
        ['email' => $email, 'source' => $source_id]
    );
    $customer_id = $customer->id;
}

$charge = \Stripe\Charge::create([
    'amount'   => $price,
    'currency' => 'usd',
    'customer' => $customer_id,
    'source'   => $source_id,
]);

echo json_encode( array('customer_id'=>$customer_id, 'source_id'=>$source_id, 'charge_id'=>$charge->id ) );

?>