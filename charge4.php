<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	require_once(__DIR__.'/inc.php');
	
	require_once(dirname(__FILE__).'/stripe/vendor/autoload.php');
	\Stripe\Stripe::setApiKey(sk_test);
	
	$_POST['vs_akkid'] = $_POST['vs_akkid'] ?? 0;
	
	try {
		$transfer = \Stripe\Transfer::create([
			'amount'         => 65,
			'currency'       => 'pln',
			'destination'    => $_POST['vs_akkid'],
			'transfer_group' => 'ORDER_95',
		]);
		
		echo json_encode(['id'=>$transfer->id]);
		
	} catch (Error $e) {
		echo json_encode(['error' => $e->getMessage()]);
	}
	
?>