<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Stripe</title>
	</head>
	<body>
		<?php
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');
			require_once(__DIR__.'/inc.php');
		?>
		
		<h1>Stripe payment with JS V3 goto STRIPE</h1>
		<p>4242424242424242</p>
		
		<button id="GoToPayStripeButt">GOTO Stripe</button>
		
		<script src="https://js.stripe.com/v3/"></script>
		<script>
			document.addEventListener("DOMContentLoaded", function(){
				var ajaxURL           = '/charge5.php';
				var GoToPayStripeButt = document.getElementById('GoToPayStripeButt');
				
				GoToPayStripeButt.addEventListener("click", function(){
					var stripe = Stripe('pk_test_51J17gXD69Y5uQACUuUiIVmv2sRig74TMJikcYiu7EUgQGKlcMg5JEDaUIQxlRlvl9JSNkBFj82TDxGxXApJjt90w004yYZFSFu');
					fetch(ajaxURL, {
						method:      'POST',
						credentials: 'same-origin',
						headers: {
							'Content-Type':  'application/x-www-form-urlencoded',
							'Cache-Control': 'no-cache',
						},
						body: new URLSearchParams({
							userID: 111,
						})
					}).then(response=>response.json())
					.then(
						response=>{
							//console.log(response);
							return stripe.redirectToCheckout({ sessionId: response.id });
						}
					)
					.catch(err=>console.log(err));
				});
				
			});
		</script>
		
	</body>
</html>