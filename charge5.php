<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	require_once(__DIR__.'/inc.php');
	
	require_once(dirname(__FILE__).'/stripe/vendor/autoload.php');
	\Stripe\Stripe::setApiKey(sk_test);
	
	
	if(isset($_GET['response']) && $_GET['response'] == 'success' && isset($_GET['session_id'])){
		echo'RESPONCE FROM STRIPEE!!!';
	}
	
	
	$userID = $_POST['userID'] ?? '';
	if($userID){
		try {
			$permalink = ( (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ?'https' :'http').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$checkout_session = \Stripe\Checkout\Session::create([
				'payment_method_types' => ['card'],
				'line_items' => [[
					'price_data' => [
						'currency'     => 'nok',
						'unit_amount'  => 33345,
						'product_data' => [
							'name'         => 'P:ay title',
							'description'  => 'Pay content',
						],
					],
					'quantity' => 1,
				]],
				/*'payment_intent_data' => [
					'application_fee_amount' => 20,
					'transfer_data' => [
						'destination' => 'acct_1J17gXD69Y5uQACU',
					],
				],*/
				'mode'           => 'payment',
				'success_url'    => $permalink . '?response=success&session_id={CHECKOUT_SESSION_ID}',
				'cancel_url'     => $permalink . '?response=cancel',
				'customer_email' => 'superpuper123@gmail.com',
			]);
			
			echo json_encode(['id'=>$checkout_session->id]);
		} catch (Error $e) {
			echo json_encode(['error' => $e->getMessage()]);
		}
	}
	
?>