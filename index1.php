<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Stripe</title>
	</head>
	<body>
		<?php
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');
			require_once(__DIR__.'/inc.php');
		?>

		
		<h1>Stripe with JS V3</h1>
		<p>4242424242424242</p>
		<script src="https://js.stripe.com/v3/"></script>
		
		
		<div>
			<div id="card-element"></div>
			<div id="card-errors" role="alert"></div>
			<a href="#uID" id="vs_submit">Submit Payment</a>
		</div>

		
		<div>
			<div id="payment-request-button">
				<!-- A Stripe Element will be inserted here. -->
			</div>
		</div>

		
		<script>
			document.addEventListener("DOMContentLoaded", function(event){
				var ajaxURL   = '/charge1.php';
				var vs_submit = document.getElementById('vs_submit');
				var stripe    = Stripe('pk_test_yPji5CZ8QtCXx3XzdRBcXdhi00qFxo11vn', {locale: 'en'});
                //https://stripe.com/docs/js/appendix/supported_locales
				var elements  = stripe.elements();
				var style = {
					base: {
						iconColor: '#00FF00',
						color: '#FF0000',
						fontWeight: '500',
						fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
						fontSize: '33px',
						fontSmoothing: 'antialiased',
						':-webkit-autofill': {
							color: '#fce883',
						},
						'::placeholder': {
							color: '#87BBFD',
						},
					},
					invalid: {
						iconColor: '#FFC7EE',
						color: '#FFC7EE',
					},
				};
				var card = elements.create('card', {style: style});
				card.mount('#card-element');
				
				
				vs_submit.addEventListener('click', function(event){

					stripe.createToken(card).then(function(result) {
						if (result.error) {
							var errorElement = document.getElementById('card-errors');
							errorElement.textContent = result.error.message;
						} else {
							console.log(result.token.id);
							var vs_token = result.token.id;

							fetch(ajaxURL, {
								method:      'POST',
								credentials: 'same-origin',
								headers: {
									'Content-Type':  'application/x-www-form-urlencoded',
									'Cache-Control': 'no-cache',
								},
								body: new URLSearchParams({
									vs_token: vs_token
								})
							}).then(response=>response.json())
							.then(
								response=>{
									console.log(response);
								}
							)
							.catch(err=>console.log(err));
						}
					});

				});
				
			});
		</script>
		
		
	</body>
</html>